//
//  AppCoordinator.swift
//  Task Manager
//
//  Created by Iryna Rivera on 10/13/20.
//

import UIKit

protocol AppCoordinatorProtocol {
    init(navController: UINavigationController)
}

class AppCoordinator: AppCoordinatorProtocol {

    let navController: UINavigationController
    
    required init(navController: UINavigationController) {
        self.navController = navController
        self.showInitialFlow()
    }
    
    private func showInitialFlow() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "LoginViewController") as! LoginViewController
        let presenter = LoginPresenter(delegate: self, apiHelper: APIHelper(), controller: controller)
        controller.presenter = presenter
        
        self.navController.viewControllers = [controller]
    }
}

extension AppCoordinator: LoginPresenterDelegate, TasksPresenterDelegate {
    func presenterDidCompleteLoginFlow(_ presenter: LoginPresenterProtocol) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "Tasks Controller") as! TasksController
        let presenter = TasksPresenter(delegate: self, apiHelper: APIHelper(), controller: controller)
        controller.presenter = presenter
        
        self.navController.pushViewController(controller, animated: true)
    }
    
    func presenterCompleteNewTaskFlow(_ presenter: TasksPresenterProtocol) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "NewTaskController") as! NewTaskController
        let presenter = NewTaskPresenter(delegate: self, apiHelper: APIHelper(), controller: controller)
        controller.presenter = presenter
        
        self.navController.pushViewController(controller, animated: true)
    }
}

extension AppCoordinator: NewTaskPresenterDelegate {
    func presenterDidCompleteNewTaskFlow(_ presenter: NewTaskPresenterProtocol) {
        self.navController.popViewController(animated: true)
    }
}
