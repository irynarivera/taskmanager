//
//  AppDelegate.swift
//  Task Manager
//
//  Created by Iryna Rivera on 9/13/20.
//

import UIKit
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var coordinator: AppCoordinatorProtocol?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let navController = UINavigationController()
        window = UIWindow()
        coordinator = AppCoordinator(navController: navController)
       
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        
        return true
    }
}

   
