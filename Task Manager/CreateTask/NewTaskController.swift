//
//  NewTaskController.swift
//  Task Manager
//
//  Created by Iryna Rivera on 9/13/20.
//

import UIKit

protocol NewTaskControllerProtocol: class {
    var presenter: NewTaskPresenterProtocol! { get set }
    
    func showAlert(title: String, message: String)
}

class NewTaskController: UIViewController, UITextViewDelegate, NewTaskControllerProtocol {
    var presenter: NewTaskPresenterProtocol!
    
    @IBOutlet weak private var taskTitle: UITextField!
    @IBOutlet weak private var contentTextView: UITextView!
    @IBOutlet weak private var dateLabel: UILabel!
    @IBOutlet weak private var datePicker: UIDatePicker!
    @IBOutlet weak private var saveBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveBtn.applyStyle()
        contentTextView.applyStyle()
        contentTextView.delegate = self
    }
    
    @IBAction private func datePickerChanged(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let strDate = dateFormatter.string(from: datePicker.date)
        dateLabel.text = strDate
        self.view.endEditing(true)
    }
    
    @IBAction private func savePressed() {
        presenter.onSaveAction(title: taskTitle.text!, status: "to do", content: contentTextView.text!, deadline: dateLabel.text!)
    }
        
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
