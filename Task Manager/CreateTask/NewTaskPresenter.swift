//
//  NewTaskPresenter.swift
//  Task Manager
//
//  Created by Iryna Rivera on 10/15/20.
//

import Foundation

protocol NewTaskPresenterProtocol: class {
    func onSaveAction(title: String, status: String, content: String, deadline: String)
}

protocol NewTaskPresenterDelegate: class {
    func presenterDidCompleteNewTaskFlow(_ presenter: NewTaskPresenterProtocol)
}

class NewTaskPresenter: NewTaskPresenterProtocol {
    private weak var delegate: NewTaskPresenterDelegate?
    private weak var controller: NewTaskControllerProtocol?
    private let apiHelper: APIHelperProtocol
    
    init(delegate: NewTaskPresenterDelegate,
         apiHelper: APIHelperProtocol,
         controller: NewTaskControllerProtocol) {
        self.delegate = delegate
        self.apiHelper = apiHelper
        self.controller = controller
    }
    
    func onSaveAction(title: String, status: String, content: String, deadline: String) {
        let url = URL(string: "https://education.octodev.net/api/v1/task")!
        let params = ["title": title, "status": status, "content": content, "deadline": deadline]
     
        apiHelper.executePOSTAPI(url, params: params) { [weak self] params, error in
            guard let self = self else { return }
            
            if let status = params?["status"] as? String, status == "ok", error == nil {
                self.delegate?.presenterDidCompleteNewTaskFlow(self)
            } else {
                self.controller?.showAlert(title: "Oops", message: "Please make sure all fields are filled")
            }
        }
    }
}
