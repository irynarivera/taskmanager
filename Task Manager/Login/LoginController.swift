//
//  ViewController.swift
//  Task Manager
//
//  Created by Iryna Rivera on 9/13/20.
//

import UIKit

protocol LoginViewControllerProtocol: class {
    var presenter: LoginPresenterProtocol! { get set }
    
    func showAlert(title: String, message: String)
}

class LoginViewController: UIViewController, UITextFieldDelegate, LoginViewControllerProtocol {    
    var presenter: LoginPresenterProtocol!
    
    @IBOutlet weak private var email: UITextField!
    @IBOutlet weak private var password: UITextField!
    @IBOutlet weak private var errorLabel: UILabel!
    @IBOutlet weak private var loginBtn: UIButton!
    @IBOutlet weak private var signupBtn: UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        loginBtn.applyStyle()
        signupBtn.applyStyle()
        
        errorLabel.text = ""
        password.delegate = self
    }
    
    @IBAction private func loginPressed() {
        if self.verifyLogin(email: email.text!, pass: password.text!) == false {
            presenter.onLoginAction(email: email.text!, password: password.text!)
        }
    }
    
    @IBAction private func signUpPressed() {
        if self.verifyLogin(email: email.text!, pass: password.text!) == false {
            presenter.onSignupAction(email: email.text!, password: password.text!)
        }
    }
    
    @IBAction private func emailEnterred() {
        if self.validateEmail(email: email.text!) == true {
            print ("Email format verified")
        } else {
            email.text = ""
            
            self.showAlert(title: "Wrong Email Format", message: "Please re-enter your email")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension LoginViewController {
    func verifyLogin(email: String, pass: String) -> Bool {
        if email.isEmpty || pass.isEmpty {
            errorLabel.text = "Please enter correct email and password"
            return true
        } else {
            return false
        }
    }
}
