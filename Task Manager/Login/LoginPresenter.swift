//
//  LoginPresenter.swift
//  Task Manager
//
//  Created by Iryna Rivera on 10/13/20.
//

import Foundation

protocol LoginPresenterProtocol: class {
    func onLoginAction(email: String, password: String)
    func onSignupAction(email: String, password: String)
}

protocol LoginPresenterDelegate: class {
    func presenterDidCompleteLoginFlow(_ presenter: LoginPresenterProtocol)
}

class LoginPresenter: LoginPresenterProtocol {
    private weak var delegate: LoginPresenterDelegate?
    private weak var controller: LoginViewControllerProtocol?
    private let apiHelper: APIHelperProtocol
    
    init(delegate: LoginPresenterDelegate,
         apiHelper: APIHelperProtocol,
         controller: LoginViewControllerProtocol) {
        self.delegate = delegate
        self.apiHelper = apiHelper
        self.controller = controller
    }
    
    func onLoginAction(email: String, password: String) {
        let url = URL(string: "https://education.octodev.net/api/v1/auth")!
        let params = ["email": email, "password": password]
            
        apiHelper.executePOSTAPI(url, params: params) { [weak self] params, error in
            guard let self = self else { return }
            
            if let token = params?["token"] as? String, error == nil {
                Token.shared.value = token
                self.delegate?.presenterDidCompleteLoginFlow(self)
            } else {
                self.controller?.showAlert(title: "Oops", message: "Your email or password is incorrect. Please try again")
            }
        }
    }
    
    func onSignupAction(email: String, password: String) {
        let url = URL(string: "https://education.octodev.net/api/v1/register")!
        let params = ["email": email, "password": password]
        
        apiHelper.executePOSTAPI(url, params: params) { [weak self] params, error in
            guard let self = self else { return }

            if let status = params!["status"] as? String, status == "ok", error == nil {
                self.controller?.showAlert(title: "Welcome!", message: "You have signed up successfully. \n Please enter your email and password to login")
            } else {
                guard let params = params else {return}
                
                if let text = params["details"] as? String {
                    self.controller?.showAlert(title: "Oops!", message: text)
                } else if let passArray = params["password"] as? [String] {
                    self.controller?.showAlert(title: "Oops!", message: passArray[0])
                } else if let emailArray = params["email"] as? [String] {
                    self.controller?.showAlert(title: "Oops!", message: emailArray[0])
                }
            }
        }
    }
}
