//
//  Task.swift
//  Task Manager
//
//  Created by Iryna Rivera on 9/29/20.
//

import Foundation

struct TaskInfo: Decodable {
    let id: Int
    let title: String
    let content: String
    let deadline: String
    var status: TaskStatus
}

enum TaskStatus: String, Decodable {
    case todo = "to do"
    case inProgress = "in progress"
    case done = "done"
    
    var intValue: Int {
        switch self {
        case .todo:
            return 0
        case .inProgress:
            return 1
        case .done:
            return 2
        }
    }
}

extension TaskInfo: Comparable {
    static func <(lhs: TaskInfo, rhs: TaskInfo) -> Bool {
        lhs.status.intValue < rhs.status.intValue
    }
}



