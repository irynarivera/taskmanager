//
//  TaskCell.swift
//  Task Manager
//
//  Created by Iryna Rivera on 9/24/20.
//

import UIKit

class TaskCell: UITableViewCell {
    @IBOutlet weak private var taskTitle: UILabel!
    @IBOutlet weak private var deadline: UILabel!
    @IBOutlet weak private var content: UITextView!
    @IBOutlet weak private var statusBtn: UIButton!
    
    var onClickAction: (() -> Void)? = nil
    
    @IBAction func statusChanged() {
        onClickAction?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        content.applyStyle()
        statusBtn.applyStyle()
    }
    
    func update(_ task: TaskInfo) {
        self.taskTitle.text = task.title
        self.deadline.text = task.deadline
        self.content.text = task.content
        self.statusBtn.setTitle(task.status.rawValue, for: .normal)
    }
}
