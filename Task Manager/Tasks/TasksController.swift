//
//  TasksController.swift
//  Task Manager
//
//  Created by Iryna Rivera on 9/13/20.
//

import UIKit

protocol TasksControllerProtocol: class {
    var presenter: TasksPresenterProtocol! { get set }
    
    func show(tasks: [TaskInfo])
    func onDeleteAction(_ taskId: Int)
    func showAlert(title: String, message: String)
}

class TasksController: UITableViewController, TasksControllerProtocol {
    var presenter: TasksPresenterProtocol!
    
    private var tasks = [TaskInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavBar()
        prepareTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        presenter.loadInitialData()
    }
    
    @IBAction private func addNewTask(_ sender: Any) {
        presenter.onNewTaskAction()
    }
    
    // MARK: - Preparations
    private func prepareNavBar() {
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "ChalkboardSE-Regular", size: 20)!]
    }
    
    private func prepareTableView() {
        tableView.tableFooterView = UIView()
    }

    func show(tasks: [TaskInfo]) {
        let tempTasks = tasks.sorted()
        self.tasks = tempTasks
        self.tableView.reloadData()
    }
    
    func onDeleteAction(_ taskId: Int) {
        let taskIndex = tasks.firstIndex(where: {$0.id == taskId})!
        let indexPath = IndexPath(row: taskIndex, section: 0)
        
        tasks.remove(at: indexPath.row)
        self.tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = tasks[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Task Cell", for: indexPath) as! TaskCell
        cell.update(model)
        cell.onClickAction = { [weak self] in
            guard let self = self else { return }
            
            self.presenter.updateStatus(id: model.id)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let model = tasks[indexPath.row]
        presenter.deleteTask(id: model.id)
    }
}
