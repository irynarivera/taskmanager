//
//  TasksPresenter.swift
//  Task Manager
//
//  Created by Iryna Rivera on 9/15/20.
//

import Foundation
import UIKit

protocol TasksPresenterProtocol: class {
    func deleteTask(id: Int)
    func onNewTaskAction()
    func loadInitialData()
    func updateStatus(id: Int)
}

protocol TasksPresenterDelegate: class {
    func presenterCompleteNewTaskFlow(_ presenter: TasksPresenterProtocol)
}

class TasksPresenter: TasksPresenterProtocol {
    private weak var delegate: TasksPresenterDelegate?
    private weak var controller: TasksControllerProtocol?
    private let apiHelper: APIHelperProtocol
    
    private var tasks: [TaskInfo] = [TaskInfo]()
    
    init(delegate: TasksPresenterDelegate,
         apiHelper: APIHelperProtocol,
         controller: TasksControllerProtocol) {
        self.delegate = delegate
        self.apiHelper = apiHelper
        self.controller = controller
    }
    
    func loadInitialData() {
        let url = URL(string: "https://education.octodev.net/api/v1/task/all")!
        
        apiHelper.executeGETAPI(url) { [weak self] arrayTasks, error  in
            guard let self = self else { return }
            
            if error == nil {
                let decoder = JSONDecoder()
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: arrayTasks as Any, options: [])
                    let tasks = try decoder.decode([TaskInfo].self, from: jsonData)

                    self.tasks = tasks
                    self.controller?.show(tasks: self.tasks)
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                self.controller?.showAlert(title: "Oops", message: "Please check your Internet connection and try again")
            }
        }
    }
    
    func updateStatus(id: Int) {
        let url = URL(string: "https://education.octodev.net/api/v1/task/update")!
        let index = tasks.firstIndex(where: { $0.id == id })!
        var task = tasks[index]
        
        switch task.status {
        case .todo:
            let params = ["id": id, "status": "in progress"] as [String : Any]
    
            apiHelper.executePOSTAPI(url, params: params) { [weak self] params, error in
                guard let self = self else { return }
            
                task.status = TaskStatus(rawValue: "in progress")!
                self.tasks[index] = task
                self.controller?.show(tasks: self.tasks)
            }
        case .inProgress:
            let params = ["id": id, "status": "done"] as [String : Any]
    
            apiHelper.executePOSTAPI(url, params: params) { [weak self] params, error in
                guard let self = self else { return }
            
                task.status = TaskStatus(rawValue: "done")!
                self.tasks[index] = task
                self.controller?.show(tasks: self.tasks)
            }
        case .done:
            let params = ["id": id, "status": "to do"] as [String : Any]
    
            apiHelper.executePOSTAPI(url, params: params) { [weak self] params, error in
                guard let self = self else { return }
            
                task.status = TaskStatus(rawValue: "to do")!
                self.tasks[index] = task
                self.controller?.show(tasks: self.tasks)
            }
        }
    }
    
    func deleteTask(id: Int) {
        let url = URL(string: "https://education.octodev.net/api/v1/task/delete")!
        let params = ["id": id]

        apiHelper.executePOSTAPI(url, params: params) { [weak self] params, error in
            guard let self = self else { return }
            
            if let status = params!["status"] as? String, status == "ok", error == nil {
                self.controller?.onDeleteAction(id)
            } else {
                print(error as Any)
            }
        }
    }
    
    func onNewTaskAction() {
        self.delegate?.presenterCompleteNewTaskFlow(self)
    }
}
