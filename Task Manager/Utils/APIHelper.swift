//
//  APIHelper.swift
//  Task Manager
//
//  Created by Iryna Rivera on 9/17/20.
//

import Foundation

protocol APIHelperProtocol: class  {
    func executeGETAPI(_ url: URL, completion: @escaping ([[String: Any]]?, Error?) -> Void)
    func executePOSTAPI(_ url: URL, params: [String: Any], completion: @escaping ([String: Any]?, Error?) -> Void)
}

class APIHelper: APIHelperProtocol {
    let session = URLSession(configuration: .default, delegate: nil, delegateQueue: OperationQueue.main)
    
    func executeGETAPI(_ url: URL, completion: @escaping ([[String: Any]]?, Error?) -> Void) {
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(Token.shared.value)", forHTTPHeaderField: "Authorization")
        
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completion(nil, error)
                return
            }
            
            let array = try! JSONSerialization.jsonObject(with: data!, options: []) as! [[String: Any]]
            completion (array, error)
        }.resume()
    }

    func executePOSTAPI(_ url: URL, params: [String: Any], completion: @escaping ([String: Any]?, Error?) -> Void) {
        let data = try? JSONSerialization.data(withJSONObject: params, options: [])
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(Token.shared.value)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.httpBody = data
        
        session.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completion(nil, error)
                return
            }
            
            let dict = try! JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
            completion(dict, error)
        }.resume()
    }
}

