//
//  Utils.swift
//  Task Manager
//
//  Created by Iryna Rivera on 9/17/20.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAlert = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed Ok")
        }
        alert.addAction(cancelAlert)
        
        self.present(alert, animated: true)
    }
    
    func validateEmail(email: String) -> Bool {
        let emailReg = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailReg)
       
        if emailPred.evaluate(with: email) {
            return true
        } else {
            return false
        }
    }
}

extension UIButton {
    func applyStyle() {
        let button = self
        button.backgroundColor = .systemBackground
        button.layer.cornerRadius = 4
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor.lightGray.cgColor
    }
}
    
extension UITextView {
    func applyStyle() {
        let text = self
        text.backgroundColor = .systemBackground
        text.layer.cornerRadius = 4
        text.layer.borderWidth = 0.5
        text.layer.borderColor = UIColor.lightGray.cgColor
    }
}



