//
//  LoginPresenterTest.swift
//  Task ManagerTests
//
//  Created by Iryna Rivera on 10/22/20.
//

import XCTest
@testable import Task_Manager

class LoginPresenterTest: XCTestCase {
    
    private var sut: LoginPresenter!
    
    private var apihelper: APIHelperMock!
    private var contoller: LoginViewControllerMock!
    private var coordinator: AppCoordinatorMock!
    

    override func setUpWithError() throws {
        apihelper = APIHelperMock()
        contoller = LoginViewControllerMock()
        coordinator = AppCoordinatorMock()
        Token.shared.value = ""
        
        sut = LoginPresenter(delegate: coordinator,
                             apiHelper: apihelper,
                             controller: contoller)
    }
    
    func test_success_login_response() {
        let token = "123"
        self.apihelper.positiveResponsePOST = ["token": token]
        
        sut.onLoginAction(email: "email", password: "pass")
        
        XCTAssertEqual(Token.shared.value, token)
        XCTAssertTrue(coordinator.didCompleteLoginFlowIsCalled)
    }
    
    func test_no_token_login_response() {
        self.apihelper.positiveResponsePOST = ["error": "erro_blabla"]
        
        sut.onLoginAction(email: "email", password: "pass")
        
        XCTAssertEqual(Token.shared.value, "")
        XCTAssertEqual(contoller.alertTitle, "Oops")
        XCTAssertEqual(contoller.alertMessage, "Your email or password is incorrect. Please try again")
    }
    
    func test_error_login_response() {
        self.apihelper.negativeResponse = NSError()
        
        sut.onLoginAction(email: "email", password: "pass")
        
        XCTAssertEqual(Token.shared.value, "")
        XCTAssertEqual(contoller.alertTitle, "Oops")
        XCTAssertEqual(contoller.alertMessage, "Your email or password is incorrect. Please try again")
    }
    
    func test_success_signup_response() {
        self.apihelper.positiveResponsePOST = ["status": "ok"]
        
        sut.onSignupAction(email: "email", password: "pass")
        
        XCTAssertEqual(contoller.alertTitle, "Welcome!")
        XCTAssertEqual(contoller.alertMessage, "You have signed up successfully. \n Please enter your email and password to login")
    }
}
