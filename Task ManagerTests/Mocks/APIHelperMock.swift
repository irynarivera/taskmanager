//
//  APIHelperMock.swift
//  Task ManagerTests
//
//  Created by Iryna Rivera on 10/22/20.
//

import Foundation
@testable import Task_Manager

class APIHelperMock: APIHelperProtocol {
    var positiveResponseGET: [[String : Any]]?
    var positiveResponsePOST: [String : Any]?
    var negativeResponse: Error?
    
    func executeGETAPI(_ url: URL, completion: @escaping ([[String : Any]]?, Error?) -> Void) {
        if let positiveResponse = positiveResponseGET {
            completion(positiveResponse, nil)
        } else if let negativeResponse = negativeResponse {
            completion(nil, negativeResponse)
        }
    }
    
    func executePOSTAPI(_ url: URL, params: [String : Any], completion: @escaping ([String : Any]?, Error?) -> Void) {
        if let positiveResponse = positiveResponsePOST {
            completion(positiveResponse, nil)
        } else if let negativeResponse = negativeResponse {
            completion(nil, negativeResponse)
        }
    }
}
