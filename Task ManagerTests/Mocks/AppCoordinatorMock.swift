//
//  AppCoordinatorMock.swift
//  Task ManagerTests
//
//  Created by Iryna Rivera on 10/22/20.
//

import Foundation
@testable import Task_Manager

class AppCoordinatorMock: LoginPresenterDelegate, TasksPresenterDelegate, NewTaskPresenterDelegate {
    
    var didCompleteLoginFlowIsCalled = false
    var completeNewTaskFlowIsCalled = false
    var didCompleteNewTaskFlowIsCalled = false
    
    func presenterDidCompleteLoginFlow(_ presenter: LoginPresenterProtocol) {
        didCompleteLoginFlowIsCalled = true
    }
    
    func presenterCompleteNewTaskFlow(_ presenter: TasksPresenterProtocol) {
        completeNewTaskFlowIsCalled = true
    }
    
    func presenterDidCompleteNewTaskFlow(_ presenter: NewTaskPresenterProtocol) {
        didCompleteNewTaskFlowIsCalled = true
    }
}
