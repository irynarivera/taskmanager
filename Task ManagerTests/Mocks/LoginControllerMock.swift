//
//  LoginControllerMock.swift
//  Task ManagerTests
//
//  Created by Iryna Rivera on 10/22/20.
//

import Foundation
@testable import Task_Manager

class LoginViewControllerMock: LoginViewControllerProtocol {
    var presenter: LoginPresenterProtocol!
    
    var alertTitle: String = ""
    var alertMessage: String = ""
    
    func showAlert(title: String, message: String) {
        alertTitle = title
        alertMessage = message
    }
}
