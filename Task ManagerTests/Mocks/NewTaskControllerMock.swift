//
//  NewTaskControllerMock.swift
//  Task ManagerTests
//
//  Created by Iryna Rivera on 10/28/20.
//

import Foundation
@testable import Task_Manager

class NewTaskControllerMock: NewTaskControllerProtocol {
    var presenter: NewTaskPresenterProtocol!
    
    var alertTitle: String = ""
    var alertMessage: String = ""
    
    func showAlert(title: String, message: String) {
        alertTitle = title
        alertMessage = message
    }
}
