//
//  TasksControllerMock.swift
//  Task ManagerTests
//
//  Created by Iryna Rivera on 10/28/20.
//

import Foundation
@testable import Task_Manager

class TasksControllerMock: TasksControllerProtocol {
    var presenter: TasksPresenterProtocol!
    
    var alertTitle: String = ""
    var alertMessage: String = ""
    var mockTasks: [TaskInfo] = []
    var taskIDToDelete: Int = 0
    
    func show(tasks: [TaskInfo]) {
        mockTasks = tasks
    }
    
    func onDeleteAction(_ taskId: Int) {
        taskIDToDelete = taskId
    }
    
    func showAlert(title: String, message: String) {
        alertTitle = title
        alertMessage = message
    }
}
