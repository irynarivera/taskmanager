//
//  NewTaskPresenterTest.swift
//  Task ManagerTests
//
//  Created by Iryna Rivera on 10/28/20.
//

import XCTest
@testable import Task_Manager

class NewTaskPresenterTest: XCTestCase {

    private var sut: NewTaskPresenter!
    
    private var apihelper: APIHelperMock!
    private var contoller: NewTaskControllerMock!
    private var coordinator: AppCoordinatorMock!
    
    override func setUpWithError() throws {
        apihelper = APIHelperMock()
        contoller = NewTaskControllerMock()
        coordinator = AppCoordinatorMock()
        
        sut = NewTaskPresenter(delegate: coordinator,
                             apiHelper: apihelper,
                             controller: contoller)
    }

    func test_success_save_action() {
        let title = "New test"
        let status = "to do"
        let content = "Task content"
        let deadline = "01-01-2022 10:10:10"
        
        self.apihelper.positiveResponsePOST = ["status": "ok"]
        
        sut.onSaveAction(title: title, status: status, content: content, deadline: deadline)
        
        XCTAssertTrue(coordinator.didCompleteNewTaskFlowIsCalled)
    }
    
    func test_error_save_action() {
        let title = "New test"
        let status = "to do"
        let content = "Task content"
        let deadline = "01-01-2022 10:10:10"
        
        self.apihelper.negativeResponse = NSError()
        
        sut.onSaveAction(title: title, status: status, content: content, deadline: deadline)
        
        XCTAssertEqual(contoller.alertTitle, "Oops")
        XCTAssertEqual(contoller.alertMessage, "Please make sure all fields are filled")
    }
}
