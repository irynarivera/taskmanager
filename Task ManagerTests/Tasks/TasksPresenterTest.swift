//
//  TasksPresenterTest.swift
//  Task ManagerTests
//
//  Created by Iryna Rivera on 10/28/20.
//

import XCTest
@testable import Task_Manager

class TasksPresenterTest: XCTestCase {

    private var sut: TasksPresenter!
    
    private var apihelper: APIHelperMock!
    private var controller: TasksControllerMock!
    private var coordinator: AppCoordinatorMock!
    
    override func setUpWithError() throws {
        apihelper = APIHelperMock()
        controller = TasksControllerMock()
        coordinator = AppCoordinatorMock()
        
        sut = TasksPresenter(delegate: coordinator,
                             apiHelper: apihelper,
                             controller: controller)
    }
    
    func test_success_load_data() {
        let taskArray: [String : Any] = ["title" : "new task",
                                         "status" : "to do",
                                         "id" : 1,
                                         "content" : "task content",
                                         "deadline" : "01-01-2022"]
        let resultArray = [taskArray, taskArray]
       
        self.apihelper.positiveResponseGET = resultArray
        self.sut.loadInitialData()
        
        for index in 0..<resultArray.count {
            let arr1 = resultArray[index]
            let arr2 = controller.mockTasks[index]
            
            XCTAssertEqual(arr2.title, arr1["title"] as! String)
            XCTAssertEqual(arr2.status.rawValue, arr1["status"] as! String)
            XCTAssertEqual(arr2.id, arr1["id"] as! Int)
            XCTAssertEqual(arr2.content, arr1["content"] as! String)
            XCTAssertEqual(arr2.deadline, arr1["deadline"] as! String)
        }
    }

    func test_error_load_data() {
        self.apihelper.negativeResponse = NSError()
        
        sut.loadInitialData()
        
        XCTAssertEqual(controller.alertTitle, "Oops")
        XCTAssertEqual(controller.alertMessage, "Please check your Internet connection and try again")
    }
    
    func test_success_delete_task() {
        let id = 25
        self.apihelper.positiveResponsePOST = ["status": "ok"]
        
        sut.deleteTask(id: id)
        
        XCTAssertEqual(controller.taskIDToDelete, id)
    }
}
